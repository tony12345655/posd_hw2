#include <vector>
#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/shape.h"
#include "../src/triangle.h"
#include "../src/rectangle.h"
#include "../src/circle.h"
#include "../src/compound_shape.h"

TEST(ShapeTest, AreaPolymorphismTest){
    Triangle tri(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    Circle cir(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    CompoundShape* compound_shape = new CompoundShape();
    std::vector<Shape*> shape_vector = {&tri, &rec, &cir, compound_shape};
    ASSERT_NEAR(tri.area(), shape_vector[0]->area(), 0.001);
    ASSERT_NEAR(rec.area(), shape_vector[1]->area(), 0.001);
    ASSERT_NEAR(cir.area(), shape_vector[2]->area(), 0.001);
    ASSERT_NEAR(compound_shape->area(), shape_vector[3]->area(), 0.001);
    delete compound_shape;
}

TEST(ShapeTest, PerimeterPolymorphismTest){
    Triangle tri(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    Circle cir(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    CompoundShape* compound_shape = new CompoundShape();
    std::vector<Shape*> shape_vector = {&tri, &rec, &cir, compound_shape};
    ASSERT_NEAR(tri.perimeter(), shape_vector[0]->perimeter(), 0.001);
    ASSERT_NEAR(rec.perimeter(), shape_vector[1]->perimeter(), 0.001);
    ASSERT_NEAR(cir.perimeter(), shape_vector[2]->perimeter(), 0.001);
    ASSERT_NEAR(compound_shape->perimeter(), shape_vector[3]->perimeter(), 0.001);
    delete compound_shape;
}

TEST(ShapeTest, InfoPolymorphismTest){
    Triangle tri(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    Circle cir(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    CompoundShape* compound_shape = new CompoundShape();
    std::vector<Shape*> shape_vector = {&tri, &rec, &cir, compound_shape};
    ASSERT_EQ(tri.info(), shape_vector[0]->info());
    ASSERT_EQ(rec.info(), shape_vector[1]->info());
    ASSERT_EQ(cir.info(), shape_vector[2]->info());
    ASSERT_EQ(compound_shape->info(), shape_vector[3]->info());
    delete compound_shape;
}

TEST(ShapeTest, AddShapePolymorphismTest){
    Triangle tri(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    Circle cir(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    CompoundShape* compound_shape = new CompoundShape();
    Shape* new_shape = new Circle(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    std::vector<Shape*> shape_vector = {&tri, &rec, &cir, compound_shape};
    ASSERT_ANY_THROW(tri.addShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[0]->addShape(new_shape));
    ASSERT_ANY_THROW(rec.addShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[1]->addShape(new_shape));
    ASSERT_ANY_THROW(cir.addShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[2]->addShape(new_shape));
    compound_shape->addShape(new_shape);
    shape_vector[3]->addShape(new_shape);
    ASSERT_NEAR(compound_shape->area(), shape_vector[3]->area(), 0.001);
    delete compound_shape;
    delete new_shape;
}

TEST(ShapeTest, DeleteShapePolymorphismTest){
    Triangle tri(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
    Rectangle rec(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(1, 0)));
    Circle cir(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    CompoundShape* compound_shape = new CompoundShape();
    Shape* new_shape = new Circle(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
    std::vector<Shape*> shape_vector = {&tri, &rec, &cir, compound_shape};
    ASSERT_ANY_THROW(tri.deleteShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[0]->deleteShape(new_shape));
    ASSERT_ANY_THROW(rec.deleteShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[1]->deleteShape(new_shape));
    ASSERT_ANY_THROW(cir.deleteShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[2]->deleteShape(new_shape));
    compound_shape->addShape(new_shape);
    shape_vector[3]->addShape(new_shape);
    compound_shape->deleteShape(new_shape);
    shape_vector[3]->deleteShape(new_shape);
    ASSERT_NEAR(compound_shape->area(), shape_vector[3]->area(), 0.001);
    delete compound_shape;
    delete new_shape;
}

