#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/shape.h"
#include "../src/triangle.h"
#include "../src/rectangle.h"
#include "../src/circle.h"
#include "../src/compound_shape.h"

class CompoundShapeTest : public ::testing::Test {
    protected:
        Triangle* tri;
        Rectangle* rec;
        Circle* cir;

        void SetUp() override {
            tri = new Triangle(new TwoDimensionalVector(new Point(0, 0), new Point(3, 0)), new TwoDimensionalVector(new Point(3, 4), new Point(3, 0)));
            rec = new Rectangle(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)), new TwoDimensionalVector(new Point(0, 0), new Point(2, 0)));
            cir = new Circle(new TwoDimensionalVector(new Point(0, 0), new Point(0, 1)));
        }

        void TearDown() override {
            delete tri;
            delete rec;
            delete cir;
        }
};

TEST_F(CompoundShapeTest, LegalTest){
    Shape* shape_arr[] = {tri, rec, cir};
    ASSERT_NO_THROW(new CompoundShape(shape_arr, 3));
}

TEST_F(CompoundShapeTest, AreaTest){
    Shape* shape_arr[] = {tri, rec, cir};
    CompoundShape compound_shape(shape_arr, 3);
    ASSERT_NEAR(8 + M_PI, compound_shape.area(), 0.001);
}

TEST_F(CompoundShapeTest, PerimeterTest){
    Shape* shape_arr[] = {tri, rec, cir};
    CompoundShape compound_shape(shape_arr, 3);
    ASSERT_NEAR(18 + 2*M_PI, compound_shape.perimeter(), 0.001);
}

TEST_F(CompoundShapeTest, InfoTest){
    Shape* shape_arr[] = {cir, rec};
    CompoundShape compound_shape(shape_arr, 2);
    ASSERT_EQ("CompoundShape (Circle (Vector ((0.00, 0.00), (0.00, 1.00))), Rectangle (Vector ((0.00, 0.00), (0.00, 1.00)), Vector ((0.00, 0.00), (2.00, 0.00))))", compound_shape.info());
}

TEST_F(CompoundShapeTest, InfoNoChildTest){
    CompoundShape* compound_shape = new CompoundShape();
    ASSERT_EQ("CompoundShape ()", compound_shape->info());
    delete compound_shape;
}

TEST_F(CompoundShapeTest, TwoCompoundShapeInfoTest){
    Shape* shape_arr1[] = {cir};
    Shape* shape_arr2[] = {rec};
    CompoundShape* compound_shape1 = new CompoundShape(shape_arr1, 1);
    CompoundShape* compound_shape2 = new CompoundShape(shape_arr2, 1);
    Shape* shape_arr3[] = {compound_shape1, compound_shape2};
    CompoundShape compound_shape3(shape_arr3, 2);
    ASSERT_EQ("CompoundShape (CompoundShape (Circle (Vector ((0.00, 0.00), (0.00, 1.00)))), CompoundShape (Rectangle (Vector ((0.00, 0.00), (0.00, 1.00)), Vector ((0.00, 0.00), (2.00, 0.00)))))", compound_shape3.info());
    delete compound_shape1;
    delete compound_shape2;
}

TEST_F(CompoundShapeTest, createDFSIteratorTest){
    Shape* shape_arr[] = {tri, rec, cir};
    CompoundShape compound_shape(shape_arr, 3);
    ASSERT_NO_THROW(compound_shape.createDFSIterator());
}

TEST_F(CompoundShapeTest, createBFSIteratorTest){
    Shape* shape_arr[] = {tri, rec, cir};
    CompoundShape compound_shape(shape_arr, 3);
    ASSERT_NO_THROW(compound_shape.createBFSIterator());
}

TEST_F(CompoundShapeTest, AddShape){
    CompoundShape* compound_shape = new CompoundShape();
    Rectangle* rec1 = new Rectangle(new TwoDimensionalVector(new Point(0, 0), new Point(0, 2)), new TwoDimensionalVector(new Point(0, 0), new Point(2, 0)));
    compound_shape->addShape(rec1);
    ASSERT_NEAR(4, compound_shape->area(), 0.001);
    delete compound_shape;
    delete rec1;
}

TEST_F(CompoundShapeTest, DeleteShape){
    Shape* shape_arr2[] = {tri, rec};
    CompoundShape* compound_shape1 = new CompoundShape(shape_arr2, 2);
    CompoundShape* compound_shape2 = new CompoundShape();
    compound_shape2->addShape(cir);
    compound_shape2->addShape(compound_shape1);
    compound_shape2->deleteShape(tri);
    ASSERT_NEAR(2 + M_PI, compound_shape2->area(), 0.001);
    delete compound_shape1;
    delete compound_shape2;
}