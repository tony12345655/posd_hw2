#include "../../src/iterator/dfs_compound_iterator.h"
#include "../../src/compound_shape.h"
#include "../../src/shape.h"
#include "../../src/rectangle.h"

class DFSCompoundIteratorTest : public ::testing::Test
{
protected:
    Point *p1, *p2, *p3, *p4;
    TwoDimensionalVector *vec1, *vec2, *vec3;
    CompoundShape *cs1, *cs2;
    Iterator* it;

    void SetUp() override
    {
        p1 = new Point(0, 0);
        p2 = new Point(0, 5);
        p3 = new Point(5, 0);
        p4 = new Point(0, 3);

        vec1 = new TwoDimensionalVector(p1, p2);
        vec2 = new TwoDimensionalVector(p1, p3);
        vec3 = new TwoDimensionalVector(p1, p4);

        cs1 = new CompoundShape();
        cs1->addShape(new Circle(vec1));
        cs1->addShape(new Rectangle(vec1,vec2));

        cs2 = new CompoundShape();
        cs2->addShape(new Circle(vec3));
        cs2->addShape(cs1);

        it = cs2->createDFSIterator();
    }

    void TearDown() override
    {
        delete cs2;
        delete p1;
        delete p2;
        delete p3;
        delete p4;
        delete vec1;
        delete vec2;
        delete vec3;
        delete it;
    }
};

TEST_F(DFSCompoundIteratorTest, CurrentItemShouldBeCorrect)
{
    ASSERT_EQ(3 * 3 * M_PI, it->currentItem()->area());
}

TEST_F(DFSCompoundIteratorTest, NextShouldBeCorrect)
{
    it->next();
    ASSERT_EQ(5 * 5 * M_PI + 25, it->currentItem()->area());
    it->next();
    ASSERT_EQ(5 * 5 * M_PI, it->currentItem()->area());
}


TEST_F(DFSCompoundIteratorTest, IsDoneShouldBeCorrect)
{
    it->next();
    it->next();
    it->next();
    it->next();

    ASSERT_TRUE(it->isDone());
}

TEST_F(DFSCompoundIteratorTest, CurrentItemShouldThrowExceptionWhenIsDone)
{
    it->next();
    it->next();
    it->next();
    it->next();

    ASSERT_ANY_THROW(it->next());
}

TEST_F(DFSCompoundIteratorTest, NextShouldThrowExceptionWhenIsDone)
{
    it->next();
    it->next();
    it->next();
    it->next();
    
    ASSERT_ANY_THROW(it->currentItem());
}

TEST_F(DFSCompoundIteratorTest, OrderShouldBeCorrectIfNoChildrenInCompound){
    CompoundShape* compound_shape = new CompoundShape();
    Iterator* it = compound_shape->createDFSIterator();
    ASSERT_TRUE(it->isDone());
    delete compound_shape;
}

TEST_F(DFSCompoundIteratorTest, ManyCompoundTest){
    CompoundShape* compound_shape_one = new CompoundShape();
    compound_shape_one->addShape(new Circle(vec1));
    compound_shape_one->addShape(new Rectangle(vec1,vec2));
    CompoundShape* compound_shape_two = new CompoundShape();
    compound_shape_two->addShape(new Circle(vec2));
    compound_shape_two->addShape(new Circle(vec3));
    CompoundShape* compound_shape_three = new CompoundShape();
    compound_shape_three->addShape(compound_shape_one);
    compound_shape_three->addShape(compound_shape_two);
    CompoundShape* compound_shape_four = new CompoundShape();
    compound_shape_four->addShape(compound_shape_two);
    compound_shape_four->addShape(compound_shape_one);
    CompoundShape* compound_shape_five = new CompoundShape();
    compound_shape_five->addShape(compound_shape_three);
    compound_shape_five->addShape(compound_shape_four);
    
    Iterator* it = compound_shape_five->createDFSIterator();
    it->next();
    it->next();
    ASSERT_EQ(5 * 5 * M_PI, it->currentItem()->area());
    it->next();
    ASSERT_EQ(25, it->currentItem()->area());
    it->next();
    ASSERT_EQ((3 * 3 + 5 * 5) * M_PI, it->currentItem()->area());

    delete compound_shape_one;
    delete compound_shape_two;
    delete compound_shape_three;
    delete compound_shape_four;
    delete compound_shape_five;

}
