#pragma once

#include "shape.h"
#include "./iterator/dfs_compound_iterator.h"
#include "./iterator/bfs_compound_iterator.h"

#include <list>
#include <stdexcept>
#include <sstream>

class CompoundShape : public Shape
{
private:
    std::list<Shape *> _shapes;

public:
    CompoundShape(Shape **shapes, int size): _shapes(shapes, shapes+size) {}

    CompoundShape() {}

    double area() const override {
        double num_area = 0;
        for (auto shape : this->_shapes)
            num_area += shape->area();
        return num_area;        
    }

    double perimeter() const override {
        double num_perimeter = 0;
        for (auto shape : this->_shapes)
            num_perimeter += shape->perimeter();
        return num_perimeter;  
    }

    std::string info() const override {
        std::string out = "CompoundShape (";
        for (auto shape : this->_shapes)
            out += (shape->info() + ", ");
        if (this->_shapes.size() != 0)
            out.erase(out.size()-2, 2);
        out += ")";
        return out;
    }

    Iterator* createDFSIterator() override {
        return new DFSCompoundIterator<std::list<Shape *>::iterator>(this->_shapes.begin(), this->_shapes.end());
    }

    Iterator* createBFSIterator() override {
        return new BFSCompoundIterator<std::list<Shape *>::iterator>(this->_shapes.begin(), this->_shapes.end());
    }

    void addShape(Shape* shape) override{
        this->_shapes.push_back(shape);
    }

    void deleteShape(Shape* shape) override{
        Shape* delete_shape = NULL;
        for(auto list_it=this->_shapes.begin(); list_it != this->_shapes.end(); ++list_it){
            Iterator* shape_it = (*list_it)->createDFSIterator();
            if ((*list_it) == shape){
                delete_shape = shape;
                break;
            }
            if (!shape_it->isDone())
                  (*list_it)->deleteShape(shape);
        }
        if (delete_shape != NULL)
            _shapes.remove(shape);
    }
};
