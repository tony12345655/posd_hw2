#pragma once

#include "shape.h"
#include "two_dimensional_vector.h"
#include "./iterator/null_iterator.h"

#include <string>
#include <sstream>

class Rectangle : public Shape
{
private:
    TwoDimensionalVector *_lengthVec;
    TwoDimensionalVector *_widthVec;

public:
    Rectangle(TwoDimensionalVector *lengthVec, TwoDimensionalVector *widthVec): _lengthVec(lengthVec), _widthVec(widthVec) {
        bool VectorLegal = *(this->_lengthVec->a()) == *(this->_widthVec-> a()) || *(this->_lengthVec->a()) == *(this->_widthVec->b()) || *(this->_lengthVec->b()) == *(this->_widthVec->a()) || *(this->_lengthVec->b()) == *(this->_widthVec->b());
        if (VectorLegal == false || this->_lengthVec->dot(_widthVec) != 0)
            throw "error";
    }
    ~Rectangle() {}

    double length() const { return _lengthVec->length(); }

    double width() const { return _widthVec->length(); }

    double area() const override {
        return this->length() * this->width();
    }

    double perimeter() const override {
        return 2 * (this->length() + this->width());
    }

    std::string info() const override {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << "Rectangle (" << this->_lengthVec->info() << ", " << this->_widthVec->info() << ")";
        std::string out = ss.str();
        
        return out; 
    }

    Iterator* createDFSIterator() override {
        return new NullIterator();
    }

    Iterator* createBFSIterator() override {
        return new NullIterator();
    }

    void addShape(Shape* shape) override {
        return throw "Rectangle can't addShape.";
    }

    void deleteShape(Shape* shape) override {
        return throw "Rectangle can't deleteShape.";
    }
};